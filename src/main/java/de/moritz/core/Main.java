package de.moritz.core;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import de.moritz.core.system.mysql.MySQLUtils;
import de.moritz.core.system.nick.commands.NameCommand;
import de.moritz.core.system.nick.commands.NickCommand;
import de.moritz.core.system.permission.listeners.PermissionListener;
import de.moritz.core.system.permission.utils.PermManager;
import de.moritz.core.system.permission.utils.PermMessageListener;
import de.moritz.core.system.player.listeners.PlayerListener;
import de.moritz.core.system.player.utils.PlayerManager;
import de.moritz.core.system.player.utils.PlayerMessageListener;
import de.moritz.core.system.server.commands.SPingCommand;
import de.moritz.core.system.team.commands.FlyCommand;
import de.moritz.core.system.team.commands.GamemodeCommand;
import de.moritz.core.system.team.commands.PluginsCommand;
import de.moritz.core.system.team.commands.SCommandsCommand;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author: toLowerCase
 */
public class Main extends JavaPlugin {

    private static Main instance;
    private static ProtocolManager protocolManager;

    public static Main getInstance( ) {
        return instance;
    }

    public static ProtocolManager getProtocolManager( ) {
        return protocolManager;
    }

    @Override
    public void onEnable( ) {
        Main.instance = this;
        Main.protocolManager = ProtocolLibrary.getProtocolManager();
        MySQLUtils.load();
        PermManager.init();
        PlayerManager.init();
        loadCommands();
        loadListeners();

        Bukkit.getMessenger().registerOutgoingPluginChannel( this, "Permission" );
        Bukkit.getMessenger().registerIncomingPluginChannel( this, "Permission", new PermMessageListener() );

        Bukkit.getMessenger().registerOutgoingPluginChannel( this, "Player" );
        Bukkit.getMessenger().registerIncomingPluginChannel( this, "Player", new PlayerMessageListener() );
    }

    @Override
    public void onDisable( ) {
        MySQLUtils.disconnect();
    }

    private void loadCommands( ) {
        new GamemodeCommand();
        new FlyCommand();
        new PluginsCommand();
        new NickCommand();
        new NameCommand();
        new SPingCommand();
        new SCommandsCommand();
    }

    private void loadListeners( ) {
        PluginManager pluginManager = getServer().getPluginManager();
        pluginManager.registerEvents( new PermissionListener(), this );
        pluginManager.registerEvents( new PlayerListener(), this );
    }
}
