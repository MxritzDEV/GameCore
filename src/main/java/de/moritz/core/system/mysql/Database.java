package de.moritz.core.system.mysql;

/**
 * @author: toLowerCase
 */
public interface Database<C> {

    void connect( );

    void disconnect( );

    void createTable( );

    boolean isConnected( );

    C getConnection( );
}