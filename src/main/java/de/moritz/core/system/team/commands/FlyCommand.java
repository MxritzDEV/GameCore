package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import org.bukkit.Bukkit;

/**
 * @author: toLowerCase
 */
public class FlyCommand extends Command {

    public FlyCommand( ) {
        super( "fly", "core.command.fly", false );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 1 ) {
            GrafikPlayer target = GrafikPlayer.getPlayer( Bukkit.getPlayer( strings[0] ) );
            if ( target == null ) {
                player.sendMessage( "§cSystem","§cDer Spieler §e" + strings[0] +
                        " §cist aktuell nicht online!" );
                return;
            }
            boolean fly = !target.getAllowFlight();
            if ( fly ) {
                player.sendMessage( "§cSystem","Der Spieler " + target.getDisplayName()
                        + " §7kann jetzt §afliegen§7." );
                target.sendMessage( "Du kannst jetzt §afliegen§7." );
            } else {
                player.sendMessage( "§cSystem","Der Spieler " + target.getDisplayName()
                        + " §7kann jetzt nicht mehr §cfliegen§7." );
                target.sendMessage( "§cSystem","Du kannst jetzt nicht mehr §cfliegen§7." );
            }
            target.setAllowFlight( fly );
            return;
        }
        if ( strings.length == 0 ) {
            boolean fly = !player.getPlayer().getAllowFlight();
            if ( fly ) {
                player.sendMessage("§cSystem", "Du kannst jetzt §afliegen§7." );
            } else {
                player.sendMessage( "§cSystem","Du kannst jetzt nicht mehr §cfliegen§7." );
            }
            player.getPlayer().setAllowFlight( fly );
            return;
        }
        player.sendMessage( "§cSystem","§cNutze§8: §c/fly §7<§fSpieler§7>" );
    }
}
