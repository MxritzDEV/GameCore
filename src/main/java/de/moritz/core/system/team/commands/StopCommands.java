package de.moritz.core.system.team.commands;

import de.moritz.core.Main;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author: toLowerCase
 */
public class StopCommands extends Command {

    public StopCommands( ) {
        super( "stop", "core.command.stop", true );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            all.kick( "§cDer Server wurde gestoppt." );
        }
        new BukkitRunnable() {
            @Override
            public void run( ) {
                Bukkit.shutdown();
            }
        }.runTaskLater( Main.getInstance(), 20 * 5 );
    }

    @Override
    public void onConsole( CommandSender commandSender, String[] strings ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            all.kick( "§cDer Server wurde gestoppt." );
        }
        new BukkitRunnable() {
            @Override
            public void run( ) {
                Bukkit.shutdown();
            }
        }.runTaskLater( Main.getInstance(), 20 * 5 );
    }
}
