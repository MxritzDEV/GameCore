package de.moritz.core.system.team.commands;

import de.moritz.core.Main;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.Plugin;

/**
 * @author: toLowerCase
 */
public class PluginsCommand extends Command implements Listener {

    public PluginsCommand( ) {
        super( "plugins", "core.command.plugins", false, "pl" );
        Bukkit.getPluginManager().registerEvents( this, Main.getInstance() );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        player.sendMessage( "§cSystem", "§7Hier eine Liste aller §ePlugins §7die auf diesem Server sind§8:" );
        for ( Plugin plugin : Bukkit.getPluginManager().getPlugins() ) {
            player.sendMessage( "    §8- §e" + plugin.getDescription().getName()
                    + " §7[" + plugin.getDescription().getVersion() + "]" );
        }
        player.sendMessage( "§cSystem","§7Der Server hat ingesammt §e" +
                Bukkit.getPluginManager().getPlugins().length + " §7Plugins." );
    }

    @EventHandler
    public void onCommand( PlayerCommandPreprocessEvent event ) {
        GrafikPlayer player = GrafikPlayer.getPlayer( event.getPlayer() );
        String command = event.getMessage();
        if ( command.equalsIgnoreCase( "/plugins" ) || command.equalsIgnoreCase( "/pl" ) ) {
            event.setCancelled( true );
            if ( !player.hasPermission( "core.command.plugins" ) ) {
                player.sendMessage( "§cSystem", "§cFür diesen Command hast du keine Rechte!" );
                if ( !player.hasPermission( "core.team.seeperm" ) )
                    return;
                player.sendMessage( "§cSystem", "§cFehlende Permission§8: §ecore.command.plugins§c!" );
                return;
            }
            player.sendMessage( "§cSystem", "§7Hier eine Liste aller §ePlugins §7die auf diesem Server sind§8:" );
            for ( Plugin plugin : Bukkit.getPluginManager().getPlugins() ) {
                player.sendMessage( "    §8- §e" + plugin.getDescription().getName()
                        + " §7[" + plugin.getDescription().getVersion() + "]" );
            }
            player.sendMessage( "§cSystem", "§7Der Server hat ingesammt §e" +
                    Bukkit.getPluginManager().getPlugins().length + " §7Plugins." );
            return;
        }
    }
}
