package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;

/**
 * @author: toLowerCase
 */
public class GamemodeCommand extends Command {

    public GamemodeCommand( ) {
        super( "gamemode", "core.command.gamemode", false, "gm" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 2 ) {
            GrafikPlayer target = GrafikPlayer.getPlayer( Bukkit.getPlayer( strings[1] ) );
            if ( target == null ) {
                player.sendMessage( "§cSystem", "§cDer Spieler §e" + strings[1] +
                        " §cist aktuell nicht online!" );
                return;
            }
            try {
                GameMode gameMode = null;
                try {
                    gameMode = GameMode.getByValue( Integer.parseInt( strings[0] ) );
                } catch ( NumberFormatException ex ) {
                    gameMode = GameMode.valueOf( strings[0].toUpperCase() );
                }
                player.sendMessage( "§cSystem", "§7Du hast den GameMode von " + target.getDisplayName()
                        + " §7zu §e" + gameMode.name() + " §7geändert." );
                target.setGameMode( gameMode );
                target.sendMessage( "§cSystem", "§7Dein GameMode hat sich zu §e" +
                        gameMode.name() + " §7geändert." );
            } catch ( IllegalArgumentException ex ) {
                player.sendMessage( "§cSystem", "§cDieser GameMode ist nicht verfügbar." );
                player.sendMessage( "§cSystem", "§7Hier eine Liste aller verfügbaren Server." );
                for ( GameMode gameMode : GameMode.values() ) {
                    player.sendMessage( "    §8- §e" + gameMode.name() + " §7[" + gameMode.getValue() + "]" );
                }
            }
            return;
        }
        if ( strings.length == 1 ) {
            try {
                GameMode gameMode = null;
                try {
                    gameMode = GameMode.getByValue( Integer.parseInt( strings[0] ) );
                } catch ( NumberFormatException ex ) {
                    gameMode = GameMode.valueOf( strings[0].toUpperCase() );
                }
                player.getPlayer().setGameMode( gameMode );
                player.sendMessage( "§cSystem", "§7Dein GameMode hat sich zu §e" +
                        gameMode.name() + " §7geändert." );
            } catch ( IllegalArgumentException ex ) {
                player.sendMessage( "§cSystem", "§cDieser GameMode ist nicht verfügbar." );
                player.sendMessage( "§cSystem", "§7Hier eine Liste aller verfügbaren Server." );
                for ( GameMode gameMode : GameMode.values() ) {
                    player.sendMessage( "    §8- §e" + gameMode.name() + " §7[" + gameMode.getValue() + "]" );
                }
            }
            return;
        }
        player.sendMessage( "§cSystem", "§cNutze§8: §c/gamemode §7[§fGameMode§7] §7<§fSpieler§7>" );
    }
}
