package de.moritz.core.system.server.utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author: toLowerCase
 */
public class ReflectionUtils {

    public static String getTexture( Player player ) {
        try {
            String craftPath = Bukkit.getServer().getClass().getPackage().getName();
            Object craftPlayer = Class.forName( String.valueOf( craftPath ) + ".entity.CraftPlayer" ).cast( (Object) player );
            Method getHandle = craftPlayer.getClass().getDeclaredMethod( "getHandle", new Class[0] );
            Object entityPlayer = getHandle.invoke( craftPlayer, new Object[0] );
            Method getProfile = entityPlayer.getClass().getMethod( "getProfile", new Class[0] );
            GameProfile profile = (GameProfile) getProfile.invoke( entityPlayer, new Object[0] );
            return profile.getProperties().get( "textures" ).iterator().next().getValue();
        } catch ( Exception ex ) {
            ex.printStackTrace();
            return null;
        }
    }

    public static Class<?> getMcClass( String name ) throws ClassNotFoundException {
        String version = Bukkit.getServer().getClass().getPackage().getName().substring( 23 );
        return Class.forName( "net.minecraft.commands." + version + "." + name );
    }

    public static Class<?> getCraftClass( String name ) throws ClassNotFoundException {
        String version = Bukkit.getServer().getClass().getPackage().getName();
        return Class.forName( String.valueOf( version ) + "." + name );
    }

    public static void set( Object obj, String name, Object value ) {
        try {
            Field field = obj.getClass().getDeclaredField( name );
            field.setAccessible( true );
            field.set( obj, value );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public static Object get( Object obj, String name ) {
        try {
            Field field = obj.getClass().getDeclaredField( name );
            field.setAccessible( true );
            return field.get( obj );
        } catch ( Exception e ) {
            e.printStackTrace();
            return null;
        }
    }

    public static Object get( Object obj, Class clazz, String name ) {
        try {
            Field field = clazz.getDeclaredField( name );
            field.setAccessible( true );
            return field.get( obj );
        } catch ( Exception e ) {
            e.printStackTrace();
            return null;
        }
    }
}
