package de.moritz.core.system.server.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;

/**
 * @author: toLowerCase
 */
public class SPingCommand extends Command {

    public SPingCommand( ) {
        super( "sping" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 1 && player.hasPermission( "core.command.ping" ) ) {
            GrafikPlayer target = GrafikPlayer.getPlayer( strings[0] );
            if ( target == null ) {
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDer Spieler §e" + strings[0] +
                        " §cist aktuell nicht online!" );
                return;
            }
            player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Der aktuelle Ping zum SpigotServer von "
                    + target.getDisplayName() + " §7beträgt§8: §e" + target.getPing() + "ms" );
            return;
        }
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Dein aktueller Ping zum SpigotServer beträgt§8: §e" +
                player.getPing() + "ms" );
    }
}
