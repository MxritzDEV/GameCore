package de.moritz.core.system.server.utils;

import com.google.common.collect.Sets;
import de.moritz.core.system.player.GrafikPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Set;

/**
 * @author: toLowerCase
 */
public abstract class Command extends BukkitCommand {

    private static Set<Command> commands = Sets.newHashSet();
    private String permission;
    private boolean console;

    public Command( String name ) {
        this( name, null, false );
    }

    public Command( String name, String permission, boolean console ) {
        this( name, permission, console, new String[0] );
    }

    public Command( String name, String permission, boolean console, String... aliases ) {
        super( name, "", "", Arrays.asList( aliases ) );
        this.permission = permission;
        this.console = console;
        registerCommand();
        Command.commands.add( this );
    }

    @Override
    public boolean execute( CommandSender commandSender, String label, String[] strings ) {
        if ( !( commandSender instanceof Player ) ) {
            if ( this.console ) {
                this.onConsole( commandSender, strings );
                return true;
            }
            commandSender.sendMessage( "§cDieser Befehl ist nur für Spieler!" );
            return true;
        }
        GrafikPlayer player = GrafikPlayer.getPlayer( (Player) commandSender );
        if ( this.permission == null || player.hasPermission( permission ) ) {
            this.onPlayer( player, strings );
            return true;
        }
        player.sendMessage( "§cSystem", "§cFür diesen Command hast du keine Rechte!" );
        if ( !player.hasPermission( "core.team.seeperm" ) )
            return true;
        player.sendMessage( "§cSystem", "§cFehlende Permission§8: §e" + this.permission + "§c!" );
        return true;
    }

    public abstract void onPlayer( GrafikPlayer player, String[] strings );

    public void onConsole( CommandSender commandSender, String[] strings ) {

    }

    private void registerCommand( ) {
        CommandMap commandMap = (CommandMap) ReflectionUtils.get( Bukkit.getServer(), "commandMap" );
        commandMap.register( this.getName(), this );
    }

    public String getPerm( ) {
        return this.permission;
    }

    public static Set<Command> getCommands( ) {
        return Command.commands;
    }
}
