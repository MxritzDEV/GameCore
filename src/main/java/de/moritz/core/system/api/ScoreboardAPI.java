package de.moritz.core.system.api;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import de.moritz.core.Main;
import net.minecraft.server.v1_8_R3.IScoreboardCriteria;
import net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardScore;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * @author: toLowerCase
 */
public class ScoreboardAPI {

    private Player player;
    private HashMap<Integer, String> lines;
    private boolean displayed;

    public ScoreboardAPI( Player player ) {
        this.player = player;
        this.lines = new HashMap<>();
        this.displayed = false;
    }

    public void sendSidebar( String displayedName ) {
        if ( this.displayed )
            return;
        PacketContainer packetScoreboard = new PacketContainer( PacketType.Play.Server.SCOREBOARD_OBJECTIVE );
        packetScoreboard.getStrings().write( 0, "sideboard" ).write( 1, displayedName );
        packetScoreboard.getIntegers().write( 0, 0 );
        packetScoreboard.getSpecificModifier( IScoreboardCriteria.EnumScoreboardHealthDisplay.class ).write( 0,
                IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER );
        PacketContainer packetPlayOut = new PacketContainer( PacketType.Play.Server.SCOREBOARD_DISPLAY_OBJECTIVE );
        packetPlayOut.getIntegers().write( 0, 1 );
        packetPlayOut.getStrings().write( 0, "sideboard" );
        try {
            Main.getProtocolManager().sendServerPacket( this.player, packetScoreboard );
            Main.getProtocolManager().sendServerPacket( this.player, packetPlayOut );
            this.displayed = true;
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
            this.displayed = false;
        }
    }

    public void remove( ) {
        PacketContainer packetRemove = new PacketContainer( PacketType.Play.Server.SCOREBOARD_OBJECTIVE );
        packetRemove.getStrings().write( 0, "sideboard" ).write( 1, "" );
        packetRemove.getIntegers().write( 0, 1 );
        try {
            Main.getProtocolManager().sendServerPacket( this.player, packetRemove );
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
        }
    }

    public void setLine( Integer line, String text ) {
        if ( !this.displayed )
            return;
        if ( text.length() > 40 )
            text = text.substring( 0, 40 );
        PacketContainer packetScore = new PacketContainer( PacketType.Play.Server.SCOREBOARD_SCORE );
        packetScore.getStrings().write( 0, text ).write( 1, "sideboard" );
        packetScore.getIntegers().write( 0, line );
        packetScore.getSpecificModifier( PacketPlayOutScoreboardScore.EnumScoreboardAction.class ).write( 0,
                PacketPlayOutScoreboardScore.EnumScoreboardAction.CHANGE );
        try {
            Main.getProtocolManager().sendServerPacket( this.player, packetScore );
            this.lines.put( line, text );
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
        }
    }

    public void removeLine( Integer line ) {
        if ( !this.displayed )
            return;
        if ( !this.lines.containsKey( line ) )
            return;
        String text = this.lines.get( line );
        PacketContainer packetScore = new PacketContainer( PacketType.Play.Server.SCOREBOARD_SCORE );
        packetScore.getStrings().write( 0, text ).write( 1, "sideboard" );
        packetScore.getIntegers().write( 0, line );
        packetScore.getSpecificModifier( PacketPlayOutScoreboardScore.EnumScoreboardAction.class ).write( 0,
                PacketPlayOutScoreboardScore.EnumScoreboardAction.REMOVE );
        try {
            Main.getProtocolManager().sendServerPacket( this.player, packetScore );
            lines.remove( line );
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
        }
    }

    public void setName( String displayedName ) {
        if ( !this.displayed )
            return;
        PacketContainer packetScoreboard = new PacketContainer( PacketType.Play.Server.SCOREBOARD_OBJECTIVE );
        packetScoreboard.getStrings().write( 0, "sideboard" ).write( 1, displayedName );
        packetScoreboard.getIntegers().write( 0, 2 );
        try {
            Main.getProtocolManager().sendServerPacket( this.player, packetScoreboard );
            this.displayed = true;
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
            this.displayed = false;
        }
    }

    public String getLine( Integer line ) {
        return this.lines.get( line );
    }
}
