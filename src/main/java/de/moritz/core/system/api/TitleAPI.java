package de.moritz.core.system.api;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import de.moritz.core.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

/**
 * @author: toLowerCase
 */
public class TitleAPI {

    public static void sendTitle( Player player, String title, String subTitle ) {
        if ( title != null ) {
            PacketContainer packetTitle = new PacketContainer( PacketType.Play.Server.TITLE );
            packetTitle.getTitleActions().write( 0, EnumWrappers.TitleAction.TITLE );
            packetTitle.getChatComponents().write( 0, WrappedChatComponent.fromJson(
                    "{\"text\": \"" + title + "\"}" ) );
            packetTitle.getIntegers().write( 0, 5 ).write( 1, 40 ).write( 2, 10 );

            try {
                Main.getProtocolManager().sendServerPacket( player, packetTitle );
            } catch ( InvocationTargetException e ) {
                e.printStackTrace();
            }
        }
        if ( subTitle != null ) {
            PacketContainer packetSubTitle = new PacketContainer( PacketType.Play.Server.TITLE );
            packetSubTitle.getTitleActions().write( 0, EnumWrappers.TitleAction.SUBTITLE );
            packetSubTitle.getChatComponents().write( 0, WrappedChatComponent.fromJson(
                    "{\"text\": \"" + subTitle + "\"}" ) );
            packetSubTitle.getIntegers().write( 0, 5 ).write( 1, 10 ).write( 2, 10 );

            try {
                Main.getProtocolManager().sendServerPacket( player, packetSubTitle );
            } catch ( InvocationTargetException e ) {
                e.printStackTrace();
            }
        }
    }

    public static void sendTitle( String title, String subTitle ) {
        Bukkit.getOnlinePlayers().forEach( all -> sendTitle( all, title, subTitle ) );
    }


}
