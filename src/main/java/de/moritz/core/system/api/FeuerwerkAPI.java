package de.moritz.core.system.api;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

/**
 * @author: toLowerCase
 */
public class FeuerwerkAPI {

    private Location location;
    private boolean flicker;
    private boolean trail;
    private FireworkEffect.Type type;
    private Color color;
    private Color fade;
    private int power;

    public FeuerwerkAPI( ) {

    }

    public void setLocation( Location location ) {
        this.location = location;
    }

    public void setFlicker( boolean flicker ) {
        this.flicker = flicker;
    }

    public void setTrail( boolean trail ) {
        this.trail = trail;
    }

    public void setType( FireworkEffect.Type type ) {
        this.type = type;
    }

    public void setColor( Color color ) {
        this.color = color;
    }

    public void setFade( Color fade ) {
        this.fade = fade;
    }

    public void setPower( int power ) {
        this.power = power;
    }

    public Location getLocation( ) {
        return this.location;
    }

    public boolean isFlicker( ) {
        return this.flicker;
    }

    public boolean isTrail( ) {
        return this.trail;
    }

    public FireworkEffect.Type getType( ) {
        return this.type;
    }

    public Color getColor( ) {
        return this.color;
    }

    public Color getFade( ) {
        return this.fade;
    }

    public int getPower( ) {
        return this.power;
    }

    public void spawn( ) {
        Firework f = this.getLocation().getWorld().spawn( this.getLocation(), Firework.class );
        FireworkMeta fm = f.getFireworkMeta();
        fm.addEffect( FireworkEffect.builder().flicker( this.isFlicker() ).trail( this.isTrail() ).with( this.getType() ).withColor( this.getColor() ).withFade( this.getFade() ).build() );
        fm.setPower( this.getPower() );
        f.setFireworkMeta( fm );
    }


}
