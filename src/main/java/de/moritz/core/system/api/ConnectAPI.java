package de.moritz.core.system.api;

import de.moritz.core.Main;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author: toLowerCase
 */
public class ConnectAPI {

    public static void joinServer( Player player, String server ) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream( byteArrayOutputStream );
            dataOutputStream.writeUTF( "Connect" );
            dataOutputStream.writeUTF( server );
            player.sendPluginMessage( Main.getInstance(), "BungeeCord", byteArrayOutputStream.toByteArray() );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}
