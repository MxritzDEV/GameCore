package de.moritz.core.system.api;

import com.google.common.collect.Maps;
import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * @author: toLowerCase
 */
public class ScoreboardBuilder {

    private Player player;
    private String title;
    private HashMap<Integer, String> lines;
    private ScoreboardAPI scoreboardAPI;

    public ScoreboardBuilder( Player player, String title ) {
        this.player = player;
        this.title = title;
        this.lines = Maps.newHashMap();
        this.scoreboardAPI = new ScoreboardAPI( player );
    }

    public void setLine( int line, String text ) {
        if ( this.lines.containsKey( line ) )
            this.scoreboardAPI.removeLine( line );
        this.lines.put( line, text );
        this.scoreboardAPI.setLine( line, text );
    }

    public void removeLine( int line ) {
        if ( !this.lines.containsKey( line ) )
            return;
        this.scoreboardAPI.removeLine( line );
        this.lines.remove( line );
    }

    public void setup( ) {
        this.scoreboardAPI.remove();
        sendTitle();
        for ( int score : this.lines.keySet() ) {
            String line = this.lines.get( score );
            this.scoreboardAPI.setLine( score, line );
        }
    }

    public void sendTitle( ) {
        this.scoreboardAPI.sendSidebar( title );
    }

    public void remove( ) {
        this.scoreboardAPI.remove();
    }

    public Player getPlayer( ) {
        return player;
    }
}
