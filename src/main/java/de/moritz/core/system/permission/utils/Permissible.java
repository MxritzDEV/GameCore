package de.moritz.core.system.permission.utils;

import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.ServerOperator;

/**
 * @author: toLowerCase
 */
public class Permissible extends PermissibleBase {

    private Player player;

    public Permissible( Player player ) {
        super( player );
        this.player = player;
    }

    @Override
    public boolean hasPermission( Permission permission ) {
        return this.hasPermission( permission.getName() );
    }

    @Override
    public boolean hasPermission( String permission ) {
        if ( permission == null ) {
            return true;
        }
        if ( permission.equalsIgnoreCase( "bukkit.broadcast.user" ) ) {
            return true;
        }
        PermUser permUser = PermUser.getUser( this.player.getUniqueId() );
        if ( permUser == null ) {
            return false;
        }
        if ( permUser.getGroup().getPermissions().contains( "*" ) ) {
            return true;
        }
        if ( permUser.getGroup().getPermissions().contains( permission ) ) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isPermissionSet( String perm ) {
        return this.hasPermission( perm );
    }
}
