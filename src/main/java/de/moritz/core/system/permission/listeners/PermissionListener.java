package de.moritz.core.system.permission.listeners;

import de.moritz.core.system.permission.utils.PermUser;
import de.moritz.core.system.permission.utils.PermissionsInjetor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author: toLowerCase
 */
public class PermissionListener implements Listener {

    @EventHandler
    public void onJoin( PlayerJoinEvent event ) {
        Player player = event.getPlayer();
        PermUser permUser = PermUser.getUser( player.getUniqueId() );
        player.setDisplayName( permUser.getGroup().getColor() + player.getName() );
        PermissionsInjetor.inject( player );
    }

    @EventHandler
    public void onQuit( PlayerQuitEvent event ) {
        Player player = event.getPlayer();
        PermUser permUser = PermUser.getUser( player.getUniqueId() );
    }
}
