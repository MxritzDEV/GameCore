package de.moritz.core.system.permission.mysql;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import de.moritz.core.system.mysql.Database;
import de.moritz.core.system.mysql.MySQLUtils;
import de.moritz.core.system.permission.utils.GroupType;
import de.moritz.core.system.permission.utils.PermGroup;
import de.moritz.core.system.permission.utils.PermUser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author: toLowerCase
 */
public class PermissionMySQL implements Database<Connection> {

    private Connection connection;
    private ExecutorService pool;

    @Override
    public void connect( ) {
        this.connection = MySQLUtils.getConnection();
        this.pool = Executors.newCachedThreadPool();
    }

    @Override
    public void disconnect( ) {
        try {
            this.connection.close();
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
    }

    @Override
    public void createTable( ) {
    }

    /*
     *  PERMISSION - GROUP
     */

    public boolean hasPermissions( PermGroup permGroup ) {
        boolean hasPermissions = false;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = this.connection.prepareStatement( "SELECT * FROM perm__permissons " +
                    "WHERE GROUPID = ?" );
            preparedStatement.setInt( 1, permGroup.getId() );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
                hasPermissions = true;
            }
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        } finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch ( SQLException e ) {
                e.printStackTrace();
            }
        }
        return hasPermissions;
    }

    private void fillPermission( PermGroup permGroup ) {
        if ( permGroup.getPermissions().isEmpty() )
            return;
        PreparedStatement preparedStatement = null;
        for ( String permission : permGroup.getPermissions() ) {
            try {
                preparedStatement = this.connection.prepareStatement( "INSERT INTO perm__permissons " +
                        "(GROUPID, PERMISSION) VALUES (?, ?)" );
                preparedStatement.setInt( 1, permGroup.getId() );
                preparedStatement.setString( 2, permission );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void clearPermission( PermGroup permGroup ) {
        if ( !hasPermissions( permGroup ) ) {
            fillPermission( permGroup );
            return;
        }
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = this.connection.prepareStatement( "DELETE FROM perm__permissons " +
                    "WHERE GROUPID = ?" );
            preparedStatement.setInt( 1, permGroup.getId() );
            preparedStatement.executeUpdate();
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch ( SQLException e ) {
                e.printStackTrace();
            }
        }
        fillPermission( permGroup );
    }


    public Set<String> getPermissions( PermGroup permGroup ) {
        Set<String> permissions = Sets.newHashSet();
        if ( !hasPermissions( permGroup ) )
            return permissions;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = this.connection.prepareStatement( "SELECT * FROM perm__permissons " +
                    "WHERE GROUPID = ?" );
            preparedStatement.setInt( 1, permGroup.getId() );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
                permissions.add( resultSet.getString( "PERMISSION" ) );
            }
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        } finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch ( SQLException e ) {
                e.printStackTrace();
            }
        }
        return permissions;
    }

    public HashMap<Integer, PermGroup> getGroups( ) {
        HashMap<Integer, PermGroup> groups = Maps.newHashMap();
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            try {
                preparedStatement = this.connection.prepareStatement( "SELECT * FROM perm__group" );
                resultSet = preparedStatement.executeQuery();
                while ( resultSet.next() ) {
                    int id = resultSet.getInt( "GROUPID" );
                    PermGroup permGroup = new PermGroup( id );
                    permGroup.setName( resultSet.getString( "GROUPNAME" ) );
                    permGroup.setShortName( resultSet.getString( "SHORTNAME" ) );
                    permGroup.setColor( resultSet.getString( "COLOR" ) );
                    permGroup.setUseFullName( resultSet.getString( "USEFULLNAME" ).equals( "true" ) );
                    try {
                        permGroup.setGroupType( GroupType.valueOf( resultSet.getString( "TYPE" ).toUpperCase() ) );
                    } catch ( IllegalArgumentException ex ) {
                        permGroup.setGroupType( GroupType.USER );
                    }
                    permGroup.setPermissions( getPermissions( permGroup ) );
                    permGroup.setDefaultGroup( resultSet.getString( "ISDEFAULT" ).equals( "true" ) );
                    groups.put( id, permGroup );
                }
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    resultSet.close();
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
        return groups;
    }

    /*
     *  PERMISSION - USER
     */

    public HashMap<UUID, PermUser> getUsers( ) {
        HashMap<UUID, PermUser> users = Maps.newHashMap();
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            try {
                preparedStatement = this.connection.prepareStatement( "SELECT * FROM perm__users" );
                resultSet = preparedStatement.executeQuery();
                while ( resultSet.next() ) {
                    UUID uuid = UUID.fromString( resultSet.getString( "UUID" ) );
                    PermUser permUser = new PermUser( uuid );
                    permUser.setName( resultSet.getString( "NAME" ) );
                    permUser.setGroup( PermGroup.getGroups().get( resultSet.getInt( "GROUPID" ) ) );
                    users.put( uuid, permUser );
                }
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    resultSet.close();
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
        return users;
    }
    /*
     *  UTILS
     */

    @Override
    public boolean isConnected( ) {
        return this.connection != null;
    }

    @Override
    public Connection getConnection( ) {
        return this.connection;
    }
}
