package de.moritz.core.system.permission.utils;

import de.moritz.core.system.mysql.MySQLUtils;

/**
 * @author: toLowerCase
 */
public class PermManager {

    public static void init( ) {
        if ( !PermGroup.getGroups().isEmpty() )
            PermGroup.getGroups().clear();
        PermGroup.setGroups( MySQLUtils.getPermissionMySQL().getGroups() );

        try {
            Thread.sleep( 1000 );
        } catch ( InterruptedException e ) {
            e.printStackTrace();
        }

        if ( !PermUser.getPermUsers().isEmpty() )
            PermUser.getPermUsers().clear();
        PermUser.setPermUsers( MySQLUtils.getPermissionMySQL().getUsers() );
    }
}
