package de.moritz.core.system.permission.utils;

import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class PermUser {

    private static HashMap<UUID, PermUser> permUsers = Maps.newHashMap();
    private UUID uuid;
    private String name;
    private PermGroup group;

    public PermUser( UUID uuid ) {
        this.uuid = uuid;
    }

    public boolean hasAccount( ) {
        return PermUser.permUsers.containsKey( uuid );
    }

    public UUID getUUID( ) {
        return this.uuid;
    }

    public String getName( ) {
        return this.name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public PermGroup getGroup( ) {
        return this.group;
    }

    public void setGroup( PermGroup group ) {
        this.group = group;
    }

    public boolean hasPermission( String permission ) {
        return this.getGroup().hasPermission( permission );
    }

    public static HashMap<UUID, PermUser> getPermUsers( ) {
        return PermUser.permUsers;
    }

    public static void setPermUsers( HashMap<UUID, PermUser> permUsers ) {
        PermUser.permUsers = permUsers;
    }

    public static PermUser getUser( UUID uuid ) {
        return PermUser.permUsers.get( uuid );
    }

    public static boolean hasAccount( UUID uuid ) {
        return PermUser.permUsers.containsKey( uuid );
    }
}
