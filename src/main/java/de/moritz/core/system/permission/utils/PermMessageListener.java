package de.moritz.core.system.permission.utils;

import de.moritz.core.system.nick.utils.UUIDFetcher;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class PermMessageListener implements PluginMessageListener {

    @Override
    public void onPluginMessageReceived( String string, Player lol, byte[] bytes ) {
        ByteArrayInputStream byteArrayInputStream = null;
        DataInputStream inputStream = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream( bytes );
            inputStream = new DataInputStream( byteArrayInputStream );
            String channel = inputStream.readUTF();
            if ( channel.equals( "reload" ) ) {
                PermManager.init();
                return;
            }
            if ( channel.equals( "createUser" ) ) {
                UUID uuid = UUID.fromString( inputStream.readUTF() );
                String name = UUIDFetcher.getName( uuid );
                PermGroup permGroup = PermGroup.getGroup( Integer.parseInt( inputStream.readUTF() ) );
                PermUser permUser = new PermUser( uuid );
                permUser.setName( name );
                permUser.setGroup( permGroup );

                PermUser.getPermUsers().put( uuid, permUser );
                return;
            }
            if ( channel.equals( "updateGroup" ) ) {
                UUID uuid = UUID.fromString( inputStream.readUTF() );
                PermGroup permGroup = PermGroup.getGroup( Integer.parseInt( inputStream.readUTF() ) );
                PermUser permUser = PermUser.getUser( uuid );
                permUser.setGroup( permGroup );
                return;
            }
            if ( channel.equals( "addPermission" ) ) {
                PermGroup permGroup = PermGroup.getGroup( Integer.parseInt( inputStream.readUTF() ) );
                String permission = inputStream.readUTF();
                permGroup.getPermissions().add( permission );
                return;
            }
            if ( channel.equals( "removePermission" ) ) {
                PermGroup permGroup = PermGroup.getGroup( Integer.parseInt( inputStream.readUTF() ) );
                String permission = inputStream.readUTF();
                permGroup.getPermissions().remove( permission );
                return;
            }
        } catch ( IOException ex ) {
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
                byteArrayInputStream.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
    }
}
