package de.moritz.core.system.permission.utils;

import de.moritz.core.system.server.utils.ReflectionUtils;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

/**
 * @author: toLowerCase
 */
public class PermissionsInjetor {

    public static void inject( Player player ) {
        try {
            Class c = ReflectionUtils.getCraftClass( "entity.CraftHumanEntity" );
            Field field = c.getDeclaredField( "perm" );
            field.setAccessible( true );
            field.set( player, new Permissible( player ) );
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }
    }

}
