package de.moritz.core.system.permission.utils;

/**
 * @author: toLowerCase
 */
public enum GroupType {

    ADMIN,
    TEAMLEITUNG,
    TEAM,
    USER;
}
