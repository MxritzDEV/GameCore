package de.moritz.core.system.permission.utils;

import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Set;

/**
 * @author: toLowerCase
 */
public class PermGroup {

    private static HashMap<Integer, PermGroup> groups = Maps.newHashMap();
    private int id;
    private String name;
    private String shortName;
    private String color;
    private boolean useFullName;
    private GroupType groupType;
    private Set<String> permissions;
    private boolean defaultGroup;

    public PermGroup( int id ) {
        this.id = id;
    }

    public static HashMap<Integer, PermGroup> getGroups( ) {
        return PermGroup.groups;
    }

    public static void setGroups( HashMap<Integer, PermGroup> groups ) {
        PermGroup.groups = groups;
    }

    public String getName( ) {
        return this.name;
    }

    public void setName( String name ) {
        name = name.replace( "%ae%", "ä" );
        this.name = name;
    }

    public int getId( ) {
        return this.id;
    }

    public void setId( int id ) {
        this.id = id;
    }

    public String getShortName( ) {
        return this.shortName;
    }

    public void setShortName( String shortName ) {
        this.shortName = shortName;
    }

    public String getColor( ) {
        return this.color.replace( "&", "§" );
    }

    public void setColor( String color ) {
        this.color = color;
    }

    public boolean isUseFullName( ) {
        return this.useFullName;
    }

    public void setUseFullName( boolean useFullName ) {
        this.useFullName = useFullName;
    }

    public GroupType getGroupType( ) {
        return this.groupType;
    }

    public void setGroupType( GroupType groupType ) {
        this.groupType = groupType;
    }

    public Set<String> getPermissions( ) {
        return this.permissions;
    }

    public void setPermissions( Set<String> permissions ) {
        this.permissions = permissions;
    }

    public boolean isDefaultGroup( ) {
        return defaultGroup;
    }

    public void setDefaultGroup( boolean defaultGroup ) {
        this.defaultGroup = defaultGroup;
    }

    public String getTeamName( ) {
        String name = "";
        int id = this.id;
        if ( id < 10 && id > 0 )
            name = "0" + id;
        else
            name = "" + id;
        name = name + this.shortName.toUpperCase();
        return name;
    }

    public static PermGroup getDefault( ) {
        PermGroup permGroup = null;
        for ( Integer id : PermGroup.groups.keySet() ) {
            if ( !PermGroup.getGroup( id ).isDefaultGroup() )
                continue;
            permGroup = PermGroup.getGroup( id );
        }
        return permGroup;
    }

    public static PermGroup getGroup( int id ) {
        PermGroup permGroup;
        try {
            permGroup = PermGroup.groups.get( id );
        } catch ( IllegalArgumentException ex ) {
            return null;
        }
        return permGroup;
    }

    public boolean hasPermission( String permission ) {
        return this.permissions.contains( permission );
    }
}