package de.moritz.core.system.nick.commands;

import de.moritz.core.system.nick.utils.NickAPI;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;

/**
 * @author: toLowerCase
 */
public class NickCommand extends Command {

    public NickCommand( ) {
        super( "nick", "core.command.nick", false );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( NickAPI.isNicked( player ) ) {
            player.unnick();
            return;
        }
        player.nick();
    }
}
