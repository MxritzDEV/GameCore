package de.moritz.core.system.nick.mysql;

import de.moritz.core.system.mysql.Database;
import de.moritz.core.system.mysql.MySQLUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author: toLowerCase
 */
public class NickMySQL implements Database<Connection> {

    private Connection connection;
    private ExecutorService pool;

    @Override
    public void connect( ) {
        this.connection = MySQLUtils.getConnection();
        this.pool = Executors.newCachedThreadPool();
        createTable();
    }

    @Override
    public void disconnect( ) {
        try {
            this.connection.close();
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        }
        this.pool.shutdown();
    }

    @Override
    public void createTable( ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = this.connection.prepareStatement( "CREATE TABLE IF NOT EXISTS nick__players (UUID VARCHAR (100), NICK VARCHAR (100), STANDARDNICK VARCHAR (100) )" );
                preparedStatement.executeUpdate();

                preparedStatement = this.connection.prepareStatement( "CREATE TABLE IF NOT EXISTS nick__nicks (UUID VARCHAR (100), NAME VARCHAR (100), SKINVALUE VARCHAR (100))" );
                preparedStatement.executeUpdate();

                preparedStatement = this.connection.prepareStatement( "CREATE TABLE IF NOT EXISTS nick__players (UUID VARCHAR (100), NICK VARCHAR (100), STANDARDNICK VARCHAR (100) )" );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    @Override
    public boolean isConnected( ) {
        return this.connection != null;
    }

    @Override
    public Connection getConnection( ) {
        return this.connection;
    }
}
