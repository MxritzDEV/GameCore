package de.moritz.core.system.nick.utils;

import com.google.common.collect.Maps;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.player.utils.ScoreboardUtils;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class NickAPI {

    private static HashMap<GrafikPlayer, NickAPI> nicks = Maps.newHashMap();
    private GrafikPlayer player;
    private GameProfile originalProfile;
    private GameProfile nickProfile;
    private String name;

    public NickAPI( GrafikPlayer player ) {
        this.player = player;
        try {
            this.originalProfile = GameProfileBuilder.fetch( player.getUUID() );
        } catch ( IOException e ) {
        }
    }

    public void nick( ) {
        changeName( this.nickProfile.getName() );
        changeSkin( this.nickProfile );
        this.player.sendMessage( "§5Nick", "§7Dein Nickname§8: §e" + this.name );

        this.player.setNick( this.name );
        ScoreboardUtils.setPlayer( this.player );
        NickAPI.nicks.put( this.player, this );
    }

    public void unnick( ) {
        changeName( this.player.getName() );
        changeSkin( this.originalProfile );
        this.player.sendMessage( "§5Nick", "§7Dein Nickname wurde entfernt." );

        this.player.setNick( null );
        ScoreboardUtils.setPlayer( this.player );
        NickAPI.nicks.remove( this.player );
    }

    private void changeName( String name ) {
        try {
            Field field = GameProfile.class.getDeclaredField( "name" );
            field.setAccessible( true );
            field.set( ( (CraftPlayer) this.player.getPlayer() ).getProfile(), name );
            field.setAccessible( false );
        } catch ( IllegalArgumentException | NoSuchFieldException | IllegalAccessException ex ) {
            ex.printStackTrace();
        }
    }

    private void changeSkin( GameProfile gameProfile ) {
        if ( gameProfile == null )
            return;
        CraftPlayer craftPlayer = (CraftPlayer) player.getPlayer();
        Collection<Property> properties = gameProfile.getProperties().get( "textures" );
        craftPlayer.getProfile().getProperties().removeAll( "textures" );
        craftPlayer.getProfile().getProperties().putAll( "textures", properties );

        PacketPlayOutEntityDestroy packetDestroy = new PacketPlayOutEntityDestroy( craftPlayer.getEntityId() );
        PacketPlayOutPlayerInfo packetPlayerInfoRemove = new PacketPlayOutPlayerInfo(
                PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, craftPlayer.getHandle() );
        PacketPlayOutPlayerInfo packetPlayerInfoAdd = new PacketPlayOutPlayerInfo(
                PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, craftPlayer.getHandle() );
        PacketPlayOutNamedEntitySpawn packetSpawn = new PacketPlayOutNamedEntitySpawn( craftPlayer.getHandle() );

        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( all.equals( this.player ) )
                continue;
            if ( all.hasPermission( "core.nick.bypass" ) )
                continue;
            all.sendPacket( packetDestroy );
            all.sendPacket( packetPlayerInfoRemove );
            all.sendPacket( packetPlayerInfoAdd );
            all.sendPacket( packetSpawn );
        }
        update();
    }

    public void setNick( String name ) {
        UUID uuid = UUIDFetcher.getUUID( name );
        try {
            this.nickProfile = GameProfileBuilder.fetch( uuid );
        } catch ( IOException ex ) {
            try {
                this.nickProfile = GameProfileBuilder.fetch( uuid );
            } catch ( IOException ex1 ) {
                this.player.sendMessage( "§5Nick", "§cEs ist ein Fehler aufgetreten!" );
                return;
            }
        }
        this.name = UUIDFetcher.getName( uuid );
    }

    public void update( ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( all.equals( this.player ) )
                continue;
            if ( all.hasPermission( "core.nick.bypass" ) )
                continue;
            if ( !all.canSee( this.player ) )
                continue;
            all.hidePlayer( this.player );
        }

        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( all.equals( this.player ) )
                continue;
            if ( all.hasPermission( "core.nick.bypass" ) )
                continue;
            if ( all.canSee( this.player ) )
                continue;
            all.showPlayer( this.player );
        }
    }

    public static boolean isNicked( GrafikPlayer player ) {
        return getNick( player ) != null;
    }

    public static NickAPI getNick( GrafikPlayer player ) {
        return NickAPI.nicks.get( player );
    }

    public static NickAPI getNick( Player player ) {
        return NickAPI.nicks.get( GrafikPlayer.getPlayer( player ) );
    }

    public static NickAPI getNick( UUID uuid ) {
        return NickAPI.nicks.get( GrafikPlayer.getPlayer( uuid ) );
    }

    public static HashMap<GrafikPlayer, NickAPI> getNicks( ) {
        return nicks;
    }
}
