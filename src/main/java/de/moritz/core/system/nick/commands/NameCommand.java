package de.moritz.core.system.nick.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import org.bukkit.Bukkit;

/**
 * @author: toLowerCase
 */
public class NameCommand extends Command {

    public NameCommand( ) {
        super( "name", "core.command.nick", false );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 0 ) {
            if ( player.isNicked() ) {
                player.sendMessage( "§5Nick", "§7Du bist genickt als " + player.getDisplayName() + "§7." );
                return;
            }
            player.sendMessage( "§5Nick", "§cDu bist nicht genickt." );
            return;
        }
        if ( strings.length == 1 ) {
            GrafikPlayer target = GrafikPlayer.getPlayer( Bukkit.getPlayer( strings[0] ) );
            if ( target == null ) {
                player.sendMessage( "§5Nick", "§cDer Spieler §e" + strings[1] +
                        " §cist aktuell nicht online!" );
                return;
            }
            if ( target.isNicked() ) {
                player.sendMessage( "§5Nick", "§7Der Spieler " + target.getRealName() +
                        " §7ist genickt als " + target.getDisplayName() + "§7." );
                return;
            }
            player.sendMessage( "§5Nick", "§cDer Spieler " + target.getRealName() + " §cist nicht genickt." );
            return;
        }
        player.sendMessage( "§5Nick", "§cNutze§8: §c/name §7<§fSpieler§7>" );
    }
}
