package de.moritz.core.system.player;

import de.moritz.core.system.api.ScoreboardBuilder;
import org.bukkit.scoreboard.Scoreboard;

import java.util.UUID;

/**
 * @author: toLowerCase
 */
public interface PlayerInfo {

    public UUID getUUID( );

    public String getName( );

    public String getIP( );

    public String getNick( );

    public int getBans( );

    public int getMutes( );

    public long getFirstJoin( );

    public long getLastLeave( );

    public long getOnlineTime( );

    public int getOnlineHours( );

}
