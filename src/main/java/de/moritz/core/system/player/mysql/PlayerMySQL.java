package de.moritz.core.system.player.mysql;

import com.google.common.collect.Maps;
import de.moritz.core.system.mysql.Database;
import de.moritz.core.system.mysql.MySQLUtils;
import de.moritz.core.system.player.GrafikOfflinePlayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author: toLowerCase
 */
public class PlayerMySQL implements Database<Connection> {

    private Connection connection;
    private ExecutorService pool;

    @Override
    public void connect( ) {
        this.connection = MySQLUtils.getConnection();
        this.pool = Executors.newCachedThreadPool();
        createTable();
    }

    @Override
    public void disconnect( ) {
        try {
            this.connection.close();
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        }
        this.pool.shutdown();
    }

    @Override
    public void createTable( ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement( "CREATE TABLE IF NOT EXISTS players__user " +
                        "(UUID VARCHAR (100), NAME VARCHAR (100), IP VARCHAR (100), BANS VARCHAR (100), " +
                        "MUTES VARCHAR (100), FIRSTJOIN VARCHAR (100), LASTLEAVE VARCHAR (100), " +
                        "ONLINETIME VARCHAR (100))" );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    public HashMap<UUID, GrafikOfflinePlayer> getOfflinePlayers( ) {
        HashMap<UUID, GrafikOfflinePlayer> offlinePlayers = Maps.newHashMap();
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            try {
                preparedStatement = this.connection.prepareStatement( "SELECT * FROM players__user" );
                resultSet = preparedStatement.executeQuery();
                while ( resultSet.next() ) {
                    UUID uuid = UUID.fromString( resultSet.getString( "UUID" ) );
                    GrafikOfflinePlayer offlinePlayer = new GrafikOfflinePlayer( uuid );
                    offlinePlayer.setName( resultSet.getString( "NAME" ) );
                    offlinePlayer.setIp( resultSet.getString( "IP" ) );
                    offlinePlayer.setBans( resultSet.getInt( "BANS" ) );
                    offlinePlayer.setMutes( resultSet.getInt( "MUTES" ) );
                    offlinePlayer.setFirstJoin( resultSet.getLong( "FIRSTJOIN" ) );
                    offlinePlayer.setLastLeave( resultSet.getLong( "LASTLEAVE" ) );
                    offlinePlayer.setOnlineTime( resultSet.getLong( "ONLINETIME" ) );
                    offlinePlayer.load();
                }
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    resultSet.close();
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
        return offlinePlayers;
    }

    @Override
    public boolean isConnected( ) {
        return this.connection != null;
    }

    @Override
    public Connection getConnection( ) {
        return this.connection;
    }
}
