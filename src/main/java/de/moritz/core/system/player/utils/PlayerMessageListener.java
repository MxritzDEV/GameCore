package de.moritz.core.system.player.utils;

import de.moritz.core.Main;
import de.moritz.core.system.player.GrafikOfflinePlayer;
import de.moritz.core.system.player.GrafikPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class PlayerMessageListener implements PluginMessageListener {

    @Override
    public void onPluginMessageReceived( String string, Player lol, byte[] bytes ) {
        ByteArrayInputStream byteArrayInputStream = null;
        DataInputStream inputStream = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream( bytes );
            inputStream = new DataInputStream( byteArrayInputStream );
            String channel = inputStream.readUTF();
            if ( channel.equals( "createPlayer" ) ) {
                UUID uuid = UUID.fromString( inputStream.readUTF() );
                GrafikOfflinePlayer offlinePlayer = new GrafikOfflinePlayer( uuid );
                offlinePlayer.create();
                return;
            }
            if ( channel.equals( "teleport" ) ) {
                GrafikPlayer player = GrafikPlayer.getPlayer( UUID.fromString( inputStream.readUTF() ) );
                GrafikPlayer target = GrafikPlayer.getPlayer( UUID.fromString( inputStream.readUTF() ) );
                new BukkitRunnable() {
                    @Override
                    public void run( ) {
                        if ( player == null )
                            return;
                        if ( target == null )
                            return;
                        player.teleport( target );
                    }
                }.runTaskLater( Main.getInstance(), 5 );
                return;
            }
        } catch ( IOException ex ) {
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
                byteArrayInputStream.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
    }
}
