package de.moritz.core.system.player;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import de.moritz.core.system.permission.utils.PermUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class GrafikOfflinePlayer implements PlayerInfo {

    private static HashMap<UUID, GrafikOfflinePlayer> grafikOfflinePlayers = Maps.newHashMap();
    private static Set<GrafikOfflinePlayer> offlinePlayers = Sets.newHashSet();
    private UUID uuid;
    private String name;
    private String ip;
    private String nick;
    private int bans;
    private int mutes;
    private long firstJoin;
    private long LastLeave;
    private long onlineTime;

    public GrafikOfflinePlayer( UUID uuid ) {
        this.uuid = uuid;
    }

    public void create( ) {
        if ( GrafikOfflinePlayer.hasAccount( this.uuid ) )
            return;
        Player player = Bukkit.getPlayer( this.uuid );
        setName( player.getName() );
        setIp( player.getAddress().getAddress().toString().replace( "/", "" ) );
        setBans( 0 );
        setMutes( 0 );
        setFirstJoin( System.currentTimeMillis() );
        setOnlineTime( 0 );
        GrafikOfflinePlayer.grafikOfflinePlayers.put( this.uuid, this );
        GrafikOfflinePlayer.offlinePlayers.add( this );

        GrafikPlayer grafikPlayer = new GrafikPlayer( player );
        grafikPlayer.login();
    }

    public void load( ) {
        GrafikOfflinePlayer.grafikOfflinePlayers.put( this.uuid, this );
        GrafikOfflinePlayer.offlinePlayers.add( this );
    }

    public void setName( String name ) {
        this.name = name;
    }

    public void setIp( String ip ) {
        this.ip = ip;
    }

    public void setNick( String nick ) {
        this.nick = nick;
    }

    public void setBans( int bans ) {
        this.bans = bans;
    }

    public void setMutes( int mutes ) {
        this.mutes = mutes;
    }

    public void setFirstJoin( long firstJoin ) {
        this.firstJoin = firstJoin;
    }

    public void setLastLeave( long lastLeave ) {
        LastLeave = lastLeave;
    }

    public void setOnlineTime( long onlineTime ) {
        this.onlineTime = onlineTime;
    }

    @Override
    public UUID getUUID( ) {
        return this.uuid;
    }

    @Override
    public String getName( ) {
        return this.name;
    }

    @Override
    public String getNick( ) {
        return this.nick;
    }

    @Override
    public String getIP( ) {
        return this.ip;
    }

    @Override
    public int getBans( ) {
        return this.bans;
    }

    @Override
    public int getMutes( ) {
        return this.mutes;
    }

    @Override
    public long getFirstJoin( ) {
        return this.firstJoin;
    }

    @Override
    public long getLastLeave( ) {
        return this.LastLeave;
    }

    @Override
    public long getOnlineTime( ) {
        return this.onlineTime;
    }

    @Override
    public int getOnlineHours( ) {
        return this.bans;
    }

    public boolean hasPermission( String permission ) {
        return PermUser.getUser( this.uuid ).hasPermission( permission );
    }

    public static boolean hasAccount( UUID uuid ) {
        return GrafikOfflinePlayer.getOfflinePlayer( uuid ) != null;
    }

    public static void setOfflinePlayers( HashMap<UUID, GrafikOfflinePlayer> grafikOfflinePlayers ) {
        GrafikOfflinePlayer.grafikOfflinePlayers = grafikOfflinePlayers;
    }

    public static GrafikOfflinePlayer getOfflinePlayer( UUID uuid ) {
        return GrafikOfflinePlayer.grafikOfflinePlayers.get( uuid );
    }

    public static Set<GrafikOfflinePlayer> getOfflinePlayers( ) {
        return GrafikOfflinePlayer.offlinePlayers;
    }
}
