package de.moritz.core.system.player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import de.moritz.core.Main;
import de.moritz.core.system.api.ActionbarAPI;
import de.moritz.core.system.api.ScoreboardBuilder;
import de.moritz.core.system.nick.utils.NickAPI;
import de.moritz.core.system.permission.utils.PermGroup;
import de.moritz.core.system.permission.utils.PermUser;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_8_R3.Packet;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scoreboard.Scoreboard;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class GrafikPlayer implements PlayerInfo {

    private static HashMap<UUID, GrafikPlayer> players = Maps.newHashMap();
    public static Set<GrafikPlayer> onlinePlayers = Sets.newHashSet();
    private Player player;
    private GrafikOfflinePlayer offlinePlayer;
    private UUID uuid;
    private String name;
    private String ip;
    private String nick;
    private Scoreboard teamScoreboard;
    private ScoreboardBuilder scoreboard;
    private int bans;
    private int mutes;
    private long firstJoin;
    private long lastLeave;
    private long onlineTime;

    public GrafikPlayer( Player player ) {
        this.player = player;
    }

    public void login( ) {
        this.offlinePlayer = GrafikOfflinePlayer.getOfflinePlayer( this.player.getUniqueId() );
        this.uuid = this.player.getUniqueId();
        this.name = this.player.getName();
        this.ip = this.player.getAddress().getAddress().toString().replace( "/", "" );
        this.teamScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.scoreboard = new ScoreboardBuilder( player.getPlayer(), "§6GrafikbugDE" );
        this.bans = this.offlinePlayer.getBans();
        this.mutes = this.offlinePlayer.getMutes();
        this.firstJoin = this.offlinePlayer.getFirstJoin();
        this.lastLeave = this.offlinePlayer.getLastLeave();
        this.onlineTime = this.offlinePlayer.getOnlineTime();

        GrafikPlayer.players.put( this.player.getUniqueId(), this );
        GrafikPlayer.onlinePlayers.add( this );
    }

    public void logut( ) {
        this.offlinePlayer.setName( this.name );
        this.offlinePlayer.setIp( this.ip );
        this.offlinePlayer.setNick( this.nick );
        this.offlinePlayer.setBans( this.bans );
        this.offlinePlayer.setMutes( this.mutes );
        this.offlinePlayer.setFirstJoin( this.firstJoin );
        this.offlinePlayer.setLastLeave( System.currentTimeMillis() );
        this.offlinePlayer.setOnlineTime( this.loadOnlineTime() );

        this.scoreboard.remove();

        GrafikPlayer.players.remove( this.uuid );
        GrafikPlayer.onlinePlayers.remove( this );
    }

    public void setNick( String nick ) {
        this.nick = nick;
    }

    public void setTeamScoreboard( Scoreboard teamScoreboard ) {
        this.player.setScoreboard( teamScoreboard );
        this.teamScoreboard = teamScoreboard;
    }

    public void setScoreboard( ScoreboardBuilder scoreboard ) {
        this.scoreboard = scoreboard;
    }

    public void setBans( int bans ) {
        this.bans = bans;
    }

    public void setMutes( int mutes ) {
        this.mutes = mutes;
    }

    public void setFirstJoin( long firstJoin ) {
        this.firstJoin = firstJoin;
    }

    public void setLastLeave( long lastLeave ) {
        lastLeave = lastLeave;
    }

    public void setOnlineTime( long onlineTime ) {
        this.onlineTime = onlineTime;
    }

    public void sendActionbar( String message ) {
        ActionbarAPI.sendActionbar( this.player, message );
    }

    public Player getPlayer( ) {
        return this.player;
    }

    public GrafikOfflinePlayer getOfflinePlayer( ) {
        return this.offlinePlayer;
    }

    @Override
    public UUID getUUID( ) {
        return this.uuid;
    }

    @Override
    public String getName( ) {
        return this.name;
    }


    public String getRealName( ) {
        return PermUser.getUser( this.uuid ).getGroup().getColor() + this.name;
    }

    @Override
    public String getIP( ) {
        return this.ip;
    }

    @Override
    public String getNick( ) {
        return this.nick;
    }

    public PermGroup getPermGroup( ) {
        if ( this.nick != null )
            return PermGroup.getDefault();
        return PermUser.getUser( this.uuid ).getGroup();
    }

    public PermGroup getRealGroup( ) {
        return PermUser.getUser( this.uuid ).getGroup();
    }

    public Scoreboard getTeamScoreboard( ) {
        return this.teamScoreboard;
    }

    public ScoreboardBuilder getScoreboard( ) {
        return this.scoreboard;
    }

    public boolean isNicked( ) {
        return NickAPI.isNicked( this );
    }

    @Override
    public int getBans( ) {
        return this.bans;
    }

    @Override
    public int getMutes( ) {
        return this.mutes;
    }

    @Override
    public long getFirstJoin( ) {
        return this.firstJoin;
    }

    @Override
    public long getLastLeave( ) {
        return this.lastLeave;
    }

    private long loadOnlineTime( ) {
        Date now = new Date();
        long time = now.getTime();
        time -= this.lastLeave;
        return time += this.onlineTime;
    }

    @Override
    public long getOnlineTime( ) {
        return this.onlineTime;
    }

    @Override
    public int getOnlineHours( ) {
        return (int) ( this.onlineTime / 3600000 );
    }

    public static GrafikPlayer getPlayer( Player player ) {
        if ( player == null )
            return null;
        return GrafikPlayer.players.get( player.getUniqueId() );
    }

    public static GrafikPlayer getPlayer( UUID uuid ) {
        if ( uuid == null )
            return null;
        return GrafikPlayer.players.get( uuid );
    }

    public static GrafikPlayer getPlayer( String name ) {
        if ( Bukkit.getPlayer( name ) == null )
            return null;
        return GrafikPlayer.getPlayer( Bukkit.getPlayer( name ) );
    }

    public static Set<GrafikPlayer> getOnlinePlayers( ) {
        return GrafikPlayer.onlinePlayers;
    }

    public boolean equals( GrafikPlayer grafikPlayer ) {
        return grafikPlayer.getName().equals( this.name ) &&
                grafikPlayer.getUUID().toString().equals( this.uuid.toString() );
    }

    //  PLAYER METHODS

    public String getDisplayName( ) {
        if ( this.nick != null )
            return PermGroup.getDefault().getColor() + this.nick;
        return PermUser.getUser( this.uuid ).getGroup().getColor() + this.name;
    }

    public boolean hasPermission( String permission ) {
        if ( this.player.hasPermission( "*" ) )
            return true;
        return this.player.hasPermission( permission );
    }

    public void sendMessage( TextComponent... textComponent ) {
        this.player.spigot().sendMessage( textComponent );
    }

    public void sendMessage( String message ) {
        this.player.sendMessage( message );
    }

    public void sendMessage( String prefix, String message ) {
        String colorCode = "c";
        if ( prefix.startsWith( "§" ) ) {
            colorCode = prefix.substring( 1, 2 );
        }
        this.player.sendMessage( "§8┃ §" + colorCode + "● §8┃ " + prefix + " §8× §7" + message );
    }

    public void sendPacket( Packet packet ) {
        ( (CraftPlayer) this.player ).getHandle().playerConnection.sendPacket( packet );
    }

    public void sendPacket( PacketContainer packetContainer ) {
        try {
            Main.getProtocolManager().sendServerPacket( this.player, packetContainer );
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
        }
    }

    public void showPlayer( GrafikPlayer player ) {
        this.player.showPlayer( player.getPlayer() );
    }

    public void hidePlayer( GrafikPlayer player ) {
        this.player.hidePlayer( player.getPlayer() );
    }

    public boolean canSee( GrafikPlayer player ) {
        return this.player.canSee( player.getPlayer() );
    }

    public Location getLocation( ) {
        return this.player.getLocation();
    }

    public void teleport( Location location ) {
        this.player.teleport( location );
    }

    public void teleport( GrafikPlayer player ) {
        this.player.teleport( player.getPlayer() );
    }

    public boolean getAllowFlight( ) {
        return this.player.getAllowFlight();
    }

    public boolean isFlying( ) {
        return this.player.isFlying();
    }

    public void setAllowFlight( boolean allowFlight ) {
        this.player.setAllowFlight( allowFlight );
    }

    public void setFlying( boolean flying ) {
        this.player.setFlying( flying );
    }

    public PlayerInventory getInventory( ) {
        return this.player.getInventory();
    }

    public void openInventory( Inventory inventory ) {
        this.player.openInventory( inventory );
    }

    public void updateInventory( ) {
        this.player.updateInventory();
    }

    public GameMode getGameMode( ) {
        return this.player.getGameMode();
    }

    public void setGameMode( GameMode gameMode ) {
        this.player.setGameMode( gameMode );
    }

    public void kick( String message ) {
        player.kickPlayer( message );
    }

    public void respawn( ) {
        Location loc = this.getLocation().clone();
        boolean allowFlight = this.getAllowFlight();
        boolean flying = this.isFlying();
        int slot = this.getInventory().getHeldItemSlot();
        PacketContainer respawnPacket = new PacketContainer( PacketType.Play.Server.RESPAWN );
        respawnPacket.getIntegers().write( 0, ( this.player.getWorld().getEnvironment().getId() ) );
        respawnPacket.getDifficulties().write( 0, EnumWrappers.Difficulty.valueOf(
                this.player.getWorld().getDifficulty().toString() ) );
        respawnPacket.getGameModes().write( 0, EnumWrappers.NativeGameMode.fromBukkit(
                this.player.getGameMode() ) );
        respawnPacket.getWorldTypeModifier().write( 0, this.player.getWorld().getWorldType() );

        PacketContainer healthUpdatePacket = new PacketContainer( PacketType.Play.Server.UPDATE_HEALTH );
        healthUpdatePacket.getFloat().write( 0, (float) this.player.getHealth() );
        healthUpdatePacket.getIntegers().write( 0, this.player.getFoodLevel() );
        healthUpdatePacket.getFloat().write( 1, this.player.getSaturation() );

        sendPacket( respawnPacket );
        sendPacket( healthUpdatePacket );
        this.teleport( loc );
        this.updateInventory();
        this.getInventory().setHeldItemSlot( slot );
        this.setAllowFlight( allowFlight );
        this.setFlying( flying );
    }

    public void realRespawn( ) {
        this.player.spigot().respawn();
    }

    public CraftPlayer getCraftPlayer( ) {
        return (CraftPlayer) this.player;
    }

    public int getPing( ) {
        return getCraftPlayer().getHandle().ping;
    }

    public void nick( ) {
        NickAPI nickAPI = new NickAPI( this );
        nickAPI.setNick( "Snoxh" );
        nickAPI.nick();
    }

    public void unnick( ) {
        NickAPI nickAPI = NickAPI.getNick( this );
        nickAPI.unnick();
    }

    public NickAPI getNickAPI( ) {
        return NickAPI.getNick( this );
    }
}
