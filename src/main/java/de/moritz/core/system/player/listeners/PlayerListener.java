package de.moritz.core.system.player.listeners;

import de.moritz.core.system.permission.utils.PermGroup;
import de.moritz.core.system.player.GrafikOfflinePlayer;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.player.utils.ScoreboardUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author: toLowerCase
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onJoin( PlayerJoinEvent event ) {
        event.setJoinMessage( null );
        Player player = event.getPlayer();
        if ( GrafikOfflinePlayer.hasAccount( player.getUniqueId() ) ) {
            GrafikPlayer grafikPlayer = new GrafikPlayer( player );
            grafikPlayer.login();

            ScoreboardUtils.setPlayer( grafikPlayer );
            ScoreboardUtils.setStandardScoreboard( grafikPlayer );
            return;
        }
        GrafikOfflinePlayer offlinePlayer = new GrafikOfflinePlayer( player.getUniqueId() );
        offlinePlayer.create();

        GrafikPlayer grafikPlayer = new GrafikPlayer( player );
        grafikPlayer.login();

        ScoreboardUtils.setPlayer( grafikPlayer );
        ScoreboardUtils.setStandardScoreboard( grafikPlayer );
    }

    @EventHandler
    public void onQuit( PlayerQuitEvent event ) {
        event.setQuitMessage( null );
        GrafikPlayer grafikPlayer = GrafikPlayer.getPlayer( event.getPlayer() );
        grafikPlayer.logut();
    }

    @EventHandler
    public void onChat( AsyncPlayerChatEvent event ) {
        GrafikPlayer player = GrafikPlayer.getPlayer( event.getPlayer() );
        PermGroup permGroup = player.getPermGroup();
        String message = event.getMessage();
        message = message.replace( "%", "Prozent" );
        if ( player.hasPermission( "core.chat.color" ) )
            message = message.replace( "&", "§" );
        event.setFormat( "§8┃ " + permGroup.getColor() + "● §8┃ " + permGroup.getColor() + permGroup.getName()
                + " §8× §7" + player.getDisplayName() + " §8➟ §f" + message );
        if ( !player.isNicked() )
            return;
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( !all.hasPermission( "core.nick.bypass" ) )
                continue;
            event.getRecipients().remove( all.getPlayer() );
            all.sendMessage( "§8┃ " + player.getRealGroup().getColor() + "● §8┃ " + player.getRealGroup().getColor()
                    + player.getRealGroup().getName() + " §8× " + player.getRealName() + " §e✎" +
                    " §8➟ §f" + message );
        }
    }
}