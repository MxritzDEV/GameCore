package de.moritz.core.system.player.utils;

import de.moritz.core.system.api.ScoreboardBuilder;
import de.moritz.core.system.permission.utils.PermGroup;
import de.moritz.core.system.player.GrafikPlayer;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * @author: toLowerCase
 */
public class ScoreboardUtils {

    public static void setTeam( GrafikPlayer player, Team team ) {
        PermGroup permGroup = player.getPermGroup();
        String prefix = "";
        if ( permGroup.isUseFullName() ) {
            prefix = permGroup.getColor() + permGroup.getShortName() + " §8× " + permGroup.getColor();
        } else {
            prefix = permGroup.getColor();
        }

        team.setPrefix( prefix );
        team.addPlayer( player.getPlayer() );
    }

    public static void setPlayer( GrafikPlayer player ) {
        updatePlayer( player );
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( all.equals( player ) )
                continue;
            PermGroup permGroup = all.getPermGroup();
            Scoreboard scoreboard = player.getTeamScoreboard();
            Team team = scoreboard.getTeam( permGroup.getTeamName() ) == null ?
                    scoreboard.registerNewTeam( permGroup.getTeamName() ) : scoreboard.getTeam( permGroup.getTeamName() );
            ScoreboardUtils.setTeam( all, team );
            player.setTeamScoreboard( scoreboard );
        }
    }

    public static void updatePlayer( GrafikPlayer player ) {
        PermGroup permGroup = player.getPermGroup();
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            Scoreboard scoreboard = all.getTeamScoreboard();
            Team team = scoreboard.getTeam( permGroup.getTeamName() ) == null ?
                    scoreboard.registerNewTeam( permGroup.getTeamName() ) : scoreboard.getTeam( permGroup.getTeamName() );
            setTeam( player, team );
            all.setTeamScoreboard( scoreboard );
        }
    }

    public static void setStandardScoreboard( GrafikPlayer player ) {
        PermGroup permGroup = player.getRealGroup();
        ScoreboardBuilder scoreboard = new ScoreboardBuilder( player.getPlayer(), "§6GrafikbugDE" );

        scoreboard.setLine( 12, "§1" );
        scoreboard.setLine( 11, "§7Dein Rang§8:" );
        scoreboard.setLine( 10, permGroup.getColor() + permGroup.getName() );
        scoreboard.setLine( 9, "§2" );
        scoreboard.setLine( 8, "§7Coins§8:" );
        scoreboard.setLine( 7, "§eCoinSystem-Abfrage" );
        scoreboard.setLine( 6, "§3" );
        scoreboard.setLine( 5, "§7Spielzeit§8:" );
        scoreboard.setLine( 4, "§a3" + ( player.getOnlineHours() == 1 ? " Stunden" : " Stunden" ) );
        scoreboard.setLine( 3, "§4" );
        scoreboard.setLine( 2, "§7TeamSpeak§8:" );
        scoreboard.setLine( 1, "§bts.Grafikbug.de" );

        scoreboard.setup();
        player.setScoreboard( scoreboard );
    }

    public static void editLine( GrafikPlayer player, int line, String text ) {
        ScoreboardBuilder scoreboard = player.getScoreboard();
        scoreboard.setLine( line, text );
    }

    public static void removeLine( GrafikPlayer player, int line ) {
        ScoreboardBuilder scoreboard = player.getScoreboard();
        scoreboard.removeLine( line );
    }
}
