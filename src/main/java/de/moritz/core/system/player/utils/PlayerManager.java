package de.moritz.core.system.player.utils;

import de.moritz.core.system.mysql.MySQLUtils;
import de.moritz.core.system.player.GrafikOfflinePlayer;

/**
 * @author: toLowerCase
 */
public class PlayerManager {

    public static void init( ) {
        GrafikOfflinePlayer.setOfflinePlayers( MySQLUtils.getPlayerMySQL().getOfflinePlayers() );
    }

}
